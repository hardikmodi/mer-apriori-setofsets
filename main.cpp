/* 
 * File:   main.cpp
 * Author: aeon
 *
 * Created on 19 December, 2012, 9:45 PM
 */

/*
 * MER Apriori algorithm to mine 'OR'ed frequent itemsets.
 * this algorithm differs from Brute Force(for frequent itemsets) only at one point. 
 * That is it maintains a list of Maximal Infrequent itemsets found uptill now and for each candicdate itemset
 * checks if it is a subset of any Maximal Infrequent Itemset.
 * 
 * In a nut shell this program carries out the following steps in a sequential manner
 * 1. Reads the dataset file and creates a BIT MAP for it
 * 2. Starts traversing the itemset lattice in top down fashion: from itemsets with greater cardinality to itemsets with less cardinality
 * 3. For each such itemset it checks if it is a subset of any Maximal Infrequent itemset if NO. then goes to the BIT MAP representation of dataset and calculates support
 * 4. If the itemset qualifies the given support threshold it adds it to the Freq Itemset Hash with its support value
 * 5. Steps 3 and 4 are repeated for all itemsets in the itemset lattice. Till this point we call it phase one
 * 6. From all the frequent itemsets found out in previous step, we then select two disjoint frequent itemsets 
 * 7. Calculate its confidence 
 * 8. if the confidence is above the required threshold, it is written into a file 
 * 9. Steps 6 to 8 are repeated for every pair of disjoint itemsets. This is called Phase 2 of the algorithm
 */


#include<stdio.h>
#include <cstdlib>
#include <iostream>
#include "Dataset.h"
#include "SetOfSet.h"
#include "MERMiner.h"
#include "time.h"
#include <sstream>
#include <string>
#include <string.h>
#define max_filepath 500
using namespace std;

void generate_outpath(char *outfilepath,char *filepath,float supp_thres,float conf_thres)
{
        char *p;
        const char *q;
        stringstream ss(stringstream::in | stringstream::out);
        string val;
        strcpy(outfilepath,filepath);
        p=strrchr(outfilepath,'/');
        p++;
        *p='\0';
        strcat(outfilepath,"mer_output_");
        ss<<supp_thres<<"_"<<conf_thres;
        val=ss.str();
        q=val.c_str();
        strcat(outfilepath,q);
        strcat(outfilepath,".txt");        
}

int main(void) 
{
    float supp_thres,conf_thres;
    long int merCount;
    char filepath[max_filepath],outfilepath[max_filepath];    
    cout<<"Path of Dataset (use forward slash): ";
    cin>>filepath;                   
    cout<<"Support Threshold: ";
    cin>>supp_thres;
    cout<<"Confidence Threshold: ";
    cin>>conf_thres;
    generate_outpath(outfilepath,filepath,supp_thres,conf_thres);    
    cout<<"\n==========MER Algo==========";
    clock_t start,end;        
    start=clock();
    Dataset D1(filepath,'\t');
    D1.summarize();
    MERMiner miner1(D1);
    merCount=miner1.mine(outfilepath,supp_thres,conf_thres);    
    end=clock();    
    printf("\n\nAlgo\t|#Items\t| #DS \t| Supp \t| Conf \t| #Rules \t| P1 Time(sec) \t| P1 Clock Ticks \t| Time(sec) \t| Clock Ticks\nMERSOS\t| %d\t| %ld\t| %.2f \t| %.2f \t| %ld\t| %0.4f\t| %.0f\t| %0.4f\t| %.0f"
            ,D1.itemsCount(),D1.transactionCount(),supp_thres,conf_thres,merCount,(float)miner1.timePhase1/CLOCKS_PER_SEC,(float)miner1.timePhase1,((float)(end-start))/CLOCKS_PER_SEC,(float)(end-start));    
    cout<<"\n\nOutput file path: "<<outfilepath<<"\n";    
    printf("\nTime Summary: ");
    printf("\n           ,ALGO      ,#Items    ,#DS       ,ParseFile     ,IBMCreate     ,getNextCandidate ,subsetCheck      ,getSupport    ,getNextRule   ,CalcConf      ,");
    printf("\ntime(sec)  ,MERSOS    ,%-10d,%-10ld,%-14.3f,%-14.3f,%-17.3f,%-17.3f,%-14.3f,%-14.3f,%-14.3f,",
            D1.itemsCount(),
            D1.transactionCount(),
            (float)D1.parseTime/CLOCKS_PER_SEC,
            (float)D1.createIBMTime/CLOCKS_PER_SEC,
            (float)miner1.timeGetNextCandidateItemset/CLOCKS_PER_SEC,
            (float)miner1.timeSubsetCheck/CLOCKS_PER_SEC,          
            (float)miner1.timeGetSupportCount/CLOCKS_PER_SEC,
            (float)miner1.timeGetNextRule/CLOCKS_PER_SEC,
            (float)miner1.timeCalculateConfidence/CLOCKS_PER_SEC);
    printf("\nclk cycles ,MERSOS    ,%-10d,%-10ld,%-14.0f,%-14.0f,%-17.0f,%-17.0f,%-14.0f,%-14.0f,%-14.0f,",
            D1.itemsCount(),
            D1.transactionCount(),
            (float)D1.parseTime,          
            (float)D1.createIBMTime,
            (float)miner1.timeGetNextCandidateItemset,
            (float)miner1.timeSubsetCheck,
            (float)miner1.timeGetSupportCount,
            (float)miner1.timeGetNextRule,
            (float)miner1.timeCalculateConfidence);
    printf("\nCalls      ,MERSOS    ,%-10d,%-10ld,%-14d,%-14d,%-17ld,%-17ld,%-14ld,%-14ld,%-14ld,"
            ,D1.itemsCount()
            ,D1.transactionCount()
            ,1
            ,1
            ,miner1.countGetNextCandidateItemset
            ,miner1.countGetNextCandidateItemset
            ,miner1.countGetSupportCount
            ,miner1.countGetNextRule
            ,miner1.countCalculateConfidence);    
    printf("\n\n");
    return 0;
}

