/* 
 * File:   Dataset.h
 * Author: aeon
 *
 * Created on 19 December, 2012, 9:28 PM
 */

#include <string>
#include <set>

using namespace std;

#ifndef DATASET_H
#define	DATASET_H
#include "time.h"

class Dataset
{
    friend class MERMiner;
    private:
        char *path;
        char delimiter;
        set<int> items;
        int no_of_items;
        long int no_of_transactions;
        bool ** ibm;
        void parseDataset(void);
        void createIBM(void);
        
    public:        
        Dataset( const char *ds_file_path,char delim);
        void summarize(void);
        ~Dataset();
        int itemsCount(void);
        long int transactionCount(void);  
        clock_t parseTime,createIBMTime;
};


#endif	/* DATASET_H */

